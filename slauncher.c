/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <magic.h>
#include <errno.h>
#include <unistd.h>
#include <linux/limits.h>

#define CONFIG_FILE ".config/slauncher"
#define COMPILED_MAGIC_DATABASE "/usr/share/misc/magic.mgc"

/*
 * Returns a heap allocated string with the mime type of filename. If there's
 * an error reading the compiled magic database, returns NULL
 */
static char *
get_mime(const char *filename)
{
	magic_t magic;
	const char *magic_mime;
	char *mime;
	int error;

	/* Load Magic database */
	magic = magic_open(MAGIC_ERROR | MAGIC_MIME_TYPE);
	if (magic_load(magic, COMPILED_MAGIC_DATABASE) == -1)
	{
		perror(COMPILED_MAGIC_DATABASE);
		magic_close(magic);
		return (NULL);
	}

	/* Get MIME */
	mime = NULL;
	if ((magic_mime = magic_file(magic, filename)))
		mime = strdup(magic_mime);
	else
	{
		if ((error = errno) == ENOENT)  /* If the file doesn't exist */
		{
			/* Check if filename is actually a URL */
			if (strncmp(filename, "http://", 7) == 0)
				mime = strdup("x-scheme-handler/http");
			else if (strncmp(filename, "https://", 8) == 0)
				mime = strdup("x-scheme-handler/https");
			else  /* Nope, it was just a non existent file */
				fprintf(stderr, "%s: %s\n", filename, strerror(error));
		}
		else
			fprintf(stderr, "%s: %s\n", filename, strerror(error));
	}
	magic_close(magic);

	return (mime);
}

/*
 * Returns a heap allocated string with the path to the config file of
 * Slauncher, or NULL if can't generate it
 */
static char *
get_config()
{
	char *config;
	static int error_already_displayed = 0;

	if (getenv("HOME"))
	{
		config = calloc(strlen(getenv("HOME")) + strlen(CONFIG_FILE) + 2, 1);
		sprintf(config, "%s/%s", getenv("HOME"), CONFIG_FILE);
		return (config);
	}
	else if (!error_already_displayed)
	{
		fprintf(stderr, "Can't get path to Slauncher's config.\n");
		fprintf(stderr, "Is the $HOME environment variable set?\n\n");
		error_already_displayed = 1;
	}

	return (NULL);
}

/*
 * This function receives a mime string (f. ex, "image/jpeg") and stores its
 * type (image) and subtype (jpeg) in the respective arguments. If there's no
 * subtype, stores NULL on its argument
 */
static void
parse_mime(const char *mime, char **type, char **subtype)
{
	size_t type_len;
	char *slash = strchr(mime, '/');

	/* Check if there's a / and therefore type/subtype, or just the type */
	type_len = slash ? (size_t)(slash - mime) : strlen(mime);
	*type = calloc(type_len + 1, 1);
	memcpy(*type, mime, type_len);

	if (slash)
	{
		*subtype = malloc(strlen(slash + 1) + 1);
		strcpy(*subtype, slash + 1);
	}
	else
		*subtype = NULL;
}

/*
 * Skipping empty lines and comments, returns a significant line read from f,
 * or NULL if EOF is reached instead
 */
static char *
read_line(FILE *f)
{
	int keep = 1;
	char *line = NULL;
	size_t n;

	while (keep)
	{
		if (getline(&line, &n, f) == -1)  /* EOF case */
		{
			free(line);
			line = NULL;
			keep = 0;
		}
		else if (strlen(line) > 1 && line[0] != '#')  /* Significant line */
			keep = 0;
		else  /* Empty line or comment */
		{
			free(line);
			line = NULL;
		}
	}

	return (line);
}

/*
 * Reads a line from the config file, and stores its command in the argument if
 * the mime type or subtype matches. NOTE: *command must be initialized to NULL
 * or an allocated string. Return codes:
 * 0: no line could be read
 * 1: mime type didn't match
 * 2: mime type matched
 * 3: both mime type and subtype matched
 */
static int
parse_line(FILE *f, char *mime_type, char *mime_subtype, char **command)
{
	char *line;
	char *line_mime, *line_mime_type, *line_mime_subtype;
	int ret_code;

	if (!(line = read_line(f)))
		return (0);

	if (strchr(line, ' ')) /* Check there's a space between mime and command */
	{
		/* Parse mime type */
		line_mime = calloc(strchr(line, ' ') - line + 1, 1);
		strncpy(line_mime, line, strchr(line, ' ') - line);
		parse_mime(line_mime, &line_mime_type, &line_mime_subtype);

		/* Mime type match, and subtype is unspecified or it matches too */
		if (strcmp(line_mime_type, mime_type) == 0 &&
				(!line_mime_subtype || (line_mime_subtype &&
				strcmp(line_mime_subtype, mime_subtype) == 0)))
		{
			/* Parse command */
			free(*command);  /* In case a previous command was allocated */

			/* Exclude \n at the end of the line */
			*command = calloc(strlen(strchr(line, ' ') + 1), 1);
			memcpy(*command, strchr(line, ' ') + 1,
					strlen(strchr(line, ' ') + 1) - 1);

			/* Check if subtype also matched */
			ret_code = line_mime_subtype &&
				strcmp(line_mime_subtype, mime_subtype) == 0 ? 3 : 2;
		}
		else
			ret_code = 1;

		free(line_mime);
		free(line_mime_type);
		free(line_mime_subtype);
		free(line);

		return (ret_code);
	}
	else  /* Treat invalid line as non match */
	{
		free(line);
		return (1);
	}
}

/*
 * Returns a heap allocated string with the command asociated to the specified
 * mime type in the Slauncher config file. Returns NULL if the mime type wasn't
 * found in the config file
 */
static char *
get_command(const char *mime)
{
	char *config;
	FILE *f;
	char *mime_type, *mime_subtype;
	char *command = NULL;
	int keep, ret_code;

	if (!(config = get_config()))
		return (NULL);

	if ((f = fopen(config, "r")))
	{
		parse_mime(mime, &mime_type, &mime_subtype);
		keep = 1;
		while (keep)
		{
			ret_code = parse_line(f, mime_type, mime_subtype, &command);

			/* EOF, or mime type and subtype match */
			if (ret_code == 0 || ret_code == 3)
				keep = 0;
		}

		fclose(f);
		free(config);
		free(mime_type);
		free(mime_subtype);

		return (command);
	}
	else
	{
		perror(config);
		free(config);
		return (NULL);
	}
}

/*
 * Launches the filename with the right command returning EXIT_SUCCESS. If
 * it fails, returns EXIT_FAILURE
 */
static int
launch(const char *filename)
{
	char *mime_type;
	char link_target[PATH_MAX];
	char *command, *full_command;
	char *config;
	int ret_code;

	if (!(mime_type = get_mime(filename)))
		return (EXIT_FAILURE);

	/* Check it its a symlink */
	if (strcmp(mime_type, "inode/symlink") == 0)
	{
		free(mime_type);
		memset(link_target, '\0', PATH_MAX);
		if (readlink(filename, link_target, PATH_MAX) == -1)
		{
			perror("readlink");
			return (EXIT_FAILURE);
		}

		return (launch(link_target));
	}

	if ((command = get_command(mime_type)))
	{
		full_command = calloc(strlen(command) + strlen(filename) + 4, 1);
		sprintf(full_command, "%s \"%s\"", command, filename);

		ret_code = system(full_command);

		free(mime_type);
		free(command);
		free(full_command);
		return (ret_code != -1 ? EXIT_SUCCESS : EXIT_FAILURE);
	}
	else
	{
		/* No point in tell to add an entry to the config file when it can't
		 * be located */
		if ((config = get_config()))
		{
			fprintf(stderr, "There isn't an entry for the MIME type %s\n",
					mime_type);
			fprintf(stderr, "You can add one to %s with the following syntax:\n\n",
					config);
			fprintf(stderr, "%s application_you_want_to_open_with\n", mime_type);
			free(config);
		}

		free(mime_type);
		return (EXIT_FAILURE);
	}
}

int
main(int argc, char *argv[])
{
	if (argc != 2)
	{
		fprintf(stderr, "Usage: %s FILE\n", argv[0]);
		return (EXIT_FAILURE);
	}

	return (launch(argv[1]));
}
