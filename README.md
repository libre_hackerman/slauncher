# Slauncher

Slauncher is a simple xdg-open replacement written in C. It reads a config file that assigns a command to open each type of file.

#### Config file

The config file path is `~/.config/slauncher`. In this file you have to write MIME types and the commands to open them, with the following syntax:

```
# Images (this is a comment)
image sxiv
image/gif mpv

# Audio
audio mpv

# Text
text gvim

# URLs
x-scheme-handler firefox --private-window
```

The config file accepts this types of entries:
- *Full mime types:* type/subtype command [options...]
- *Generalistic mime types:* type command [options...]
- Empty lines
- Comments (lines that start with a #)

Full mime types will override generalistic ones of the same type (in the example above, `image/gif` command overrides the `image` one)

#### MIME types

Slauncher uses the MIME types provided by `file`. So in order to know the MIME type of a particular file, you can retrieve it with any of this ways:

- Using Slauncher itself. When trying to open a file with a MIME not stored in its config, it will notify the user:
```
$ slauncher picture.jpg
There isn't an entry for the MIME type image/jpeg
You can add one to /home/user/.config/slauncher with the following syntax:

image/jpeg application_you_want_to_open_with
```
- Using `file`:
```
$ file --mime-type picture.jpg
picture.jpg: image/jpeg
```

One exception to the `file` approach are http/https URLs. `file` doesn't manage them, so I borrowed `x-scheme-handler/http` and `x-scheme-handler/https` from XDG to be the MIME types for URLs.

### Manual build

#### Dependencies
- Make
- GCC/Clang
- File

#### Build
Build with `make`. That will leave you the `slauncher` executable.

#### Installation
You can directly run the compiled executable, but if you want to install it
on your system, `make install` will do it. Notice that you can set the
variable `PREFIX` (default to /usr/local) to your desire.

*NOTE*: If you want Slauncher to be executed instead of xdg-open, you can override it creating the following symlink:
```
# ln -s `which slauncher` /usr/local/bin/xdg-open
```

#### Uninstall
`make uninstall` (Mind the `PREFIX` too)
